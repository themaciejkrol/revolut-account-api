# Account API

The general task was to: *"Design and implement a RESTful API (including data model and the backing implementation) for money transfers between accounts."*.

I believe that programme is only as good as the communication around it. Your opinion is extremely important to me, so I hope to speak with you soon, discuss and always find new ways to improve.

**Assumptions:**

* All amounts are represented as integers. Think of cents - in case of dollars. In general any minimal unit of a given currency.
* All dates in JSON are UNIX timestamps.

## Fast start

```bash
> cd /project/path
> mvn package
> java -jar target/account-api-1.0.jar
```

Server is started by default at 127.0.0.1:8080

## API Functionality

All requests and responses are in JSON. The API allows for:

### Creation of the account

`GET /account/create` 

### Retrieval of account data
`GET /account/get/{uuid}`

### Listing of available accounts

`POST /list`

Request body must contain `limit` parameter and may contain `offset` parameter.

**Example request body:**

```json
{
    "limit": 3,
    "offset": 2
}
```

### Making transfer between accounts

`POST /account/transfer`

**Example request body:**

```json
{
    "source": "91a0f1e9-ce32-4159-8af2-25a8851be958",
    "destination": "97520f4a-273c-4d7a-b399-96bfece61e31",
    "amount": 100
}
```

### Listing transfers
`POST /transfer/list`

Request body must contain `limit` parameter and may contain `offset` parameter.

**Example request body:**

```json
{
    "limit": 3,
    "offset": 2
}
```

Additionally, transfers can be filtered using more optional parameters. See example body:

```json
{
    "limit": 3,
    "offset": 2,
    "query" : {
        "source": "91a0f1e9-ce32-4159-8af2-25a8851be958",
        "destination": "97520f4a-273c-4d7a-b399-96bfece61e31",
        "dateFrom": 1228137326467,
        "dateTo": 1528137326467
    }
}
```

## Building and running application

The application can be built using Maven `mvn package`. This will create a `account-api-{version}.jar` file in the target directory. 

By default, the HTTP server is started on port 8080 and the API is populated with the example dataset. Both default config and accounts dataset are stored in the [resources/default](resources/default).

Alternatively, one can provide their own configuration (file) or (file and accounts file) for preloading data. See [resources/default](resources/default) for reference.

**Example:**

```bash
java -jar target/account-api-{version}.jar \
/path/to/server/config.json \
/path/to/server/accountsFile.json
```

## Tests

Before running unit tests, make sure that ports in range 8080-8090 are not in use. They are used to startup server for testing.

I am aware that not all cases are covered by tests. However, taking into consideration this is only to give you the impression of my coding abilities and style, I think that writing more would only delay posting this task without giving you more insight about me as a developer.

## Have a nice day.
