package pl.maciejkrol.revolut.accountapi;

import pl.maciejkrol.revolut.accountapi.common.ResourceUtil;
import pl.maciejkrol.revolut.accountapi.dao.AccountDAO;
import pl.maciejkrol.revolut.accountapi.dao.TransferDAO;
import pl.maciejkrol.revolut.accountapi.dao.inmemory.InMemoryAccountDAO;
import pl.maciejkrol.revolut.accountapi.dao.inmemory.InMemoryTransferDAO;
import pl.maciejkrol.revolut.accountapi.entity.Account;
import pl.maciejkrol.revolut.accountapi.service.SimpleTransferService;
import pl.maciejkrol.revolut.accountapi.service.TransferService;
import pl.maciejkrol.revolut.accountapi.web.Server;
import io.undertow.Undertow;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class Main {

    public static void main(final String... args) throws IOException {

        final JSONObject config = new JSONObject(
                (args.length > 0)
                        ? new String(Files.readAllBytes(Paths.get(args[0])))
                        : ResourceUtil.toString("/default/config.json")
        );

        final JSONArray accountsJSON = new JSONArray(
                (args.length > 1)
                        ? new String(Files.readAllBytes(Paths.get(args[1])))
                        : ResourceUtil.toString("/default/accounts.json")
        );

        final Set<Account> accounts = new HashSet<>();
        accountsJSON.forEach(object -> accounts.add(
                new Account(
                        UUID.fromString(((JSONObject) object).getString("id")),
                        ((JSONObject) object).getLong("balance")
                )
        ));

        //////////////////

        final AccountDAO accountDAO = new InMemoryAccountDAO(accounts);
        final TransferDAO transferDAO = new InMemoryTransferDAO();
        final TransferService transferService = new SimpleTransferService(accountDAO, transferDAO);

        final Undertow undertow = Server.prepare(accountDAO, transferDAO, transferService)
                .addHttpListener(config.getInt("port"), config.getString("host"))
                .setIoThreads(config.getInt("ioThreads"))
                .setWorkerThreads(config.getInt("workerThreads"))
                .build();

        undertow.start();

        Runtime.getRuntime().addShutdownHook(new Thread(undertow::stop));
    }
}