package pl.maciejkrol.revolut.accountapi.dao;

import pl.maciejkrol.revolut.accountapi.dao.fault.transfer.TransferCreationException;
import pl.maciejkrol.revolut.accountapi.dao.fault.transfer.TransferNotFoundException;
import pl.maciejkrol.revolut.accountapi.entity.Transfer;
import pl.maciejkrol.revolut.accountapi.entity.TransferSearchArgument;

import java.util.List;
import java.util.UUID;

public interface TransferDAO {

    default boolean exists(UUID uuid) {
        try {
            this.get(uuid);
            return true;
        } catch (TransferNotFoundException ignored) {
            return false;
        }
    }

    Transfer get(UUID uuid) throws TransferNotFoundException;

    Transfer register(UUID source, UUID destination, long amount) throws TransferCreationException;

    List<Transfer> list(TransferSearchArgument searchArgument, long offset, int limit);
}