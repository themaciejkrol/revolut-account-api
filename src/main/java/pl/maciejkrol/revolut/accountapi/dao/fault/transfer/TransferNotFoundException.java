package pl.maciejkrol.revolut.accountapi.dao.fault.transfer;

import pl.maciejkrol.revolut.accountapi.dao.fault.TransferException;

import java.util.UUID;

public class TransferNotFoundException extends TransferException {

    private final UUID uuid;

    public TransferNotFoundException(final UUID uuid) {
        this.uuid = uuid;
    }

    public UUID getUuid() {
        return uuid;
    }
}