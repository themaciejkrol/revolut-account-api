package pl.maciejkrol.revolut.accountapi.dao;

import pl.maciejkrol.revolut.accountapi.dao.fault.TransferException;
import pl.maciejkrol.revolut.accountapi.dao.fault.account.AccountCreationException;
import pl.maciejkrol.revolut.accountapi.dao.fault.account.AccountNotFoundException;
import pl.maciejkrol.revolut.accountapi.entity.Account;

import java.util.List;
import java.util.UUID;

public interface AccountDAO {

    Account openAccount() throws AccountCreationException;

    default boolean exists(UUID uuid) {
        try {
            this.get(uuid);
            return true;
        } catch (AccountNotFoundException ignored) {
            return false;
        }
    }

    Account get(UUID uuid) throws AccountNotFoundException;

    default void transfer(UUID sourceUuid, UUID destinationUuid, long amount) throws AccountNotFoundException, TransferException {
        this.transfer(sourceUuid, destinationUuid, amount, false);
    }

    void transfer(UUID sourceUuid, UUID destinationUuid, long amount, boolean allowDebt) throws AccountNotFoundException, TransferException;

    List<Account> list(long offset, int limit);
}