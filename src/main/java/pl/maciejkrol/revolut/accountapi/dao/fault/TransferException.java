package pl.maciejkrol.revolut.accountapi.dao.fault;

public class TransferException extends Exception {

    public TransferException() {
    }

    public TransferException(final String message) {
        super(message);
    }

    public TransferException(final Throwable cause) {
        super(cause);
    }
}
