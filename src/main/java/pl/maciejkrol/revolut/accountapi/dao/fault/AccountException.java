package pl.maciejkrol.revolut.accountapi.dao.fault;

public class AccountException extends Exception {

    public AccountException() {
    }
    
    public AccountException(final Throwable cause) {
        super(cause);
    }
}