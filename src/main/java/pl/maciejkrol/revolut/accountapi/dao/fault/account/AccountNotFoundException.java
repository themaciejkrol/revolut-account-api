package pl.maciejkrol.revolut.accountapi.dao.fault.account;

import pl.maciejkrol.revolut.accountapi.dao.fault.AccountException;

import java.util.UUID;

public class AccountNotFoundException extends AccountException {

    private final UUID uuid;

    public AccountNotFoundException(final UUID uuid) {
        this.uuid = uuid;
    }

    public UUID getUuid() {
        return uuid;
    }
}