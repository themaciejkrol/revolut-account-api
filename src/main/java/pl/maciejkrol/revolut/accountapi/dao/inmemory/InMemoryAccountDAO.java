package pl.maciejkrol.revolut.accountapi.dao.inmemory;

import pl.maciejkrol.revolut.accountapi.dao.AccountDAO;
import pl.maciejkrol.revolut.accountapi.dao.fault.TransferException;
import pl.maciejkrol.revolut.accountapi.dao.fault.account.AccountCreationException;
import pl.maciejkrol.revolut.accountapi.dao.fault.account.AccountNotFoundException;
import pl.maciejkrol.revolut.accountapi.dao.fault.account.InsufficientFundsException;
import pl.maciejkrol.revolut.accountapi.dao.fault.account.InvalidAmountException;
import pl.maciejkrol.revolut.accountapi.dao.inmemory.fault.UUIDCollisionException;
import pl.maciejkrol.revolut.accountapi.entity.Account;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class InMemoryAccountDAO implements AccountDAO {

    private final Map<UUID, Account> accounts = new ConcurrentHashMap<>();

    public InMemoryAccountDAO() {
    }

    public InMemoryAccountDAO(final Set<Account> accounts) {
        for (final Account account : accounts) {
            this.accounts.put(account.getUuid(), account);
        }
    }

    @Override
    public synchronized Account openAccount() throws AccountCreationException {
        int retries = 3;
        UUID uuid;

        do {
            retries--;
            uuid = UUID.randomUUID();
            if (!this.exists(uuid)) {
                final Account account = new Account(uuid, 0);
                this.putAccount(account);
                return account;
            }
        } while (retries > 0);

        throw new AccountCreationException(new UUIDCollisionException());
    }

    @Override
    public Account get(final UUID uuid) throws AccountNotFoundException {
        final Account account = this.accounts.getOrDefault(uuid, null);

        if (account == null) {
            throw new AccountNotFoundException(uuid);
        } else {
            return Account.clone(account);
        }
    }

    @Override
    public synchronized void transfer(
            final UUID sourceUuid,
            final UUID destinationUuid,
            final long amount,
            final boolean allowDebt
    ) throws AccountNotFoundException, TransferException {
        if (amount <= 0) {
            throw new InvalidAmountException();
        }

        Account sourceAccount = this.get(sourceUuid);

        if (!allowDebt) {
            if (sourceAccount.getBalance() < amount) {
                throw new InsufficientFundsException();
            }
        }

        Account destinationAccount = this.get(destinationUuid);

        sourceAccount = new Account(
                sourceAccount.getUuid(),
                sourceAccount.getBalance() - amount
        );
        destinationAccount = new Account(
                destinationAccount.getUuid(),
                destinationAccount.getBalance() + amount
        );

        this.putAccount(sourceAccount);
        this.putAccount(destinationAccount);
    }

    @Override
    public List<Account> list(final long offset, final int limit) {
        return this.accounts
                .values()
                .stream()
                .sorted(Comparator.comparing(Account::getUuid))
                .skip(offset)
                .limit(limit)
                .collect(Collectors.toList());
    }

    private void putAccount(final Account account) {
        this.accounts.put(account.getUuid(), account);
    }
}