package pl.maciejkrol.revolut.accountapi.dao.fault.transfer;

import pl.maciejkrol.revolut.accountapi.dao.fault.TransferException;

public class TransferCreationException extends TransferException {

    public TransferCreationException(final Throwable cause) {
        super(cause);
    }
}