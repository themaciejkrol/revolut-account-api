package pl.maciejkrol.revolut.accountapi.dao.fault.account;

import pl.maciejkrol.revolut.accountapi.dao.fault.TransferException;

public class InsufficientFundsException extends TransferException {
}
