package pl.maciejkrol.revolut.accountapi.dao.inmemory;

import pl.maciejkrol.revolut.accountapi.dao.TransferDAO;
import pl.maciejkrol.revolut.accountapi.dao.fault.transfer.TransferCreationException;
import pl.maciejkrol.revolut.accountapi.dao.fault.transfer.TransferNotFoundException;
import pl.maciejkrol.revolut.accountapi.dao.inmemory.fault.UUIDCollisionException;
import pl.maciejkrol.revolut.accountapi.entity.Transfer;
import pl.maciejkrol.revolut.accountapi.entity.TransferSearchArgument;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class InMemoryTransferDAO implements TransferDAO {

    private final Map<UUID, Transfer> transfers = new ConcurrentHashMap<>();

    @Override
    public Transfer get(final UUID uuid) throws TransferNotFoundException {
        final Transfer transfer = this.transfers.getOrDefault(uuid, null);

        if (transfer == null) {
            throw new TransferNotFoundException(uuid);
        } else {
            return Transfer.clone(transfer);
        }
    }

    @Override
    public synchronized Transfer register(final UUID source, final UUID destination, final long amount) throws TransferCreationException {
        int retries = 3;
        UUID uuid;

        do {
            retries--;
            uuid = UUID.randomUUID();
            if (!this.exists(uuid)) {
                final Transfer transfer = new Transfer(uuid, source, destination, new Date(), amount);
                this.putTransfer(transfer);
                return transfer;
            }
        } while (retries > 0);

        throw new TransferCreationException(new UUIDCollisionException());
    }

    @Override
    public List<Transfer> list(final TransferSearchArgument searchArgument, final long offset, final int limit) {
        Stream<Transfer> stream = this.transfers
                .values()
                .stream();

        if (searchArgument.getSource() != null) {
            stream = stream.filter(transfer -> transfer.getSource().equals(searchArgument.getSource()));
        }

        if (searchArgument.getDestination() != null) {
            stream = stream.filter(transfer -> transfer.getDestination().equals(searchArgument.getDestination()));
        }

        if (searchArgument.getDateFrom() != null) {
            stream = stream.filter(transfer -> transfer.getDate().getTime() >= searchArgument.getDateFrom().getTime());
        }

        if (searchArgument.getDateTo() != null) {
            stream = stream.filter(transfer -> transfer.getDate().getTime() <= searchArgument.getDateTo().getTime());
        }

        return stream
                .sorted(Comparator.comparing(Transfer::getDate))
                .skip(offset)
                .limit(limit)
                .collect(Collectors.toList());
    }

    private void putTransfer(final Transfer transfer) {
        this.transfers.put(transfer.getUuid(), transfer);
    }
}