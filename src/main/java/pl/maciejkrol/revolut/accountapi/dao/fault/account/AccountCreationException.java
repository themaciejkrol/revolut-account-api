package pl.maciejkrol.revolut.accountapi.dao.fault.account;

import pl.maciejkrol.revolut.accountapi.dao.fault.AccountException;

public class AccountCreationException extends AccountException {

    public AccountCreationException(final Throwable cause) {
        super(cause);
    }
}