package pl.maciejkrol.revolut.accountapi.web.handler;

import io.undertow.server.HttpServerExchange;
import org.json.JSONArray;
import org.json.JSONObject;
import pl.maciejkrol.revolut.accountapi.dao.AccountDAO;
import pl.maciejkrol.revolut.accountapi.entity.Account;
import pl.maciejkrol.revolut.accountapi.web.fault.MalformedPayloadException;
import pl.maciejkrol.revolut.accountapi.web.handler.general.JSONPayloadHTTPHandler;
import pl.maciejkrol.revolut.accountapi.web.handler.general.JSONResponseHttpHandler;

import java.io.IOException;
import java.util.List;

public class AccountListHttpHandler implements JSONPayloadHTTPHandler {

    private final AccountDAO accountDAO;

    public AccountListHttpHandler(final AccountDAO accountDAO) {
        this.accountDAO = accountDAO;
    }

    @Override
    public void handleRequest(final HttpServerExchange exchange) throws IOException, MalformedPayloadException {
        final JSONObject payload = this.getPayload(exchange);

        final long offset = payload.has("offset")
                ? payload.getLong("offset")
                : 0;

        if (!payload.has("limit")) {
            throw new MalformedPayloadException();
        }

        final int limit = payload.getInt("limit");

        final List<Account> accounts = this.accountDAO.list(offset, limit);
        final JSONArray accountsJSON = new JSONArray();

        accounts.forEach(account -> accountsJSON.put(
                new JSONObject()
                        .put("id", account.getUuid().toString())
                        .put("balance", account.getBalance())
        ));

        final JSONObject response = new JSONObject()
                .put("success", true)
                .put("payload", accountsJSON);

        new JSONResponseHttpHandler(response).handleRequest(exchange);
    }
}