package pl.maciejkrol.revolut.accountapi.web.handler;

import pl.maciejkrol.revolut.accountapi.dao.fault.AccountException;
import pl.maciejkrol.revolut.accountapi.dao.fault.TransferException;
import pl.maciejkrol.revolut.accountapi.entity.Transfer;
import pl.maciejkrol.revolut.accountapi.service.TransferService;
import pl.maciejkrol.revolut.accountapi.web.fault.MalformedPayloadException;
import pl.maciejkrol.revolut.accountapi.web.handler.general.JSONPayloadHTTPHandler;
import pl.maciejkrol.revolut.accountapi.web.handler.general.JSONResponseHttpHandler;
import io.undertow.server.HttpServerExchange;
import org.json.JSONObject;

import java.io.IOException;
import java.util.UUID;

public class TransferHttpHandler implements JSONPayloadHTTPHandler {

    private final TransferService transferService;

    public TransferHttpHandler(final TransferService transferService) {
        this.transferService = transferService;
    }

    @Override
    public void handleRequest(final HttpServerExchange exchange) throws IOException, MalformedPayloadException, TransferException, AccountException {
        final JSONObject payload = this.getPayload(exchange);

        if (!payload.has("source") || !payload.has("destination") || !payload.has("amount")) {
            throw new MalformedPayloadException();
        }

        if (payload.isNull("amount")) {
            throw new MalformedPayloadException();
        }

        final UUID sourceUuid = UUID.fromString(payload.getString("source"));
        final UUID destinationUuid = UUID.fromString(payload.getString("destination"));
        final long amount = payload.getLong("amount");

        final Transfer transfer = this.transferService.transfer(sourceUuid, destinationUuid, amount);

        final JSONObject response = new JSONObject()
                .put("success", true)
                .put("payload", new JSONObject()
                        .put("uuid", transfer.getUuid())
                );

        new JSONResponseHttpHandler(response).handleRequest(exchange);
    }
}