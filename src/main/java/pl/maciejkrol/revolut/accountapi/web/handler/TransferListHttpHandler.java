package pl.maciejkrol.revolut.accountapi.web.handler;

import pl.maciejkrol.revolut.accountapi.dao.TransferDAO;
import pl.maciejkrol.revolut.accountapi.entity.Transfer;
import pl.maciejkrol.revolut.accountapi.entity.TransferSearchArgument;
import pl.maciejkrol.revolut.accountapi.web.fault.MalformedPayloadException;
import pl.maciejkrol.revolut.accountapi.web.handler.general.JSONPayloadHTTPHandler;
import pl.maciejkrol.revolut.accountapi.web.handler.general.JSONResponseHttpHandler;
import io.undertow.server.HttpServerExchange;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class TransferListHttpHandler implements JSONPayloadHTTPHandler {

    private final TransferDAO transferDAO;

    public TransferListHttpHandler(final TransferDAO transferDAO) {
        this.transferDAO = transferDAO;
    }

    @Override
    public void handleRequest(final HttpServerExchange exchange) throws IOException, MalformedPayloadException {
        final JSONObject payload = this.getPayload(exchange);
        final TransferSearchArgument transferSearchArgument = this.prepareTransferSearchArgument(payload);

        final long offset = payload.has("offset")
                ? payload.getLong("offset")
                : 0;

        if (!payload.has("limit")) {
            throw new MalformedPayloadException();
        }

        final int limit = payload.getInt("limit");

        final List<Transfer> transfers = this.transferDAO.list(transferSearchArgument, offset, limit);

        final JSONArray transfersJSON = new JSONArray();

        transfers.forEach(transfer -> transfersJSON.put(
                new JSONObject()
                        .put("id", transfer.getUuid().toString())
                        .put("source", transfer.getSource().toString())
                        .put("destination", transfer.getDestination().toString())
                        .put("date", transfer.getDate().getTime())
                        .put("amount", transfer.getAmount())
        ));

        final JSONObject response = new JSONObject()
                .put("success", true)
                .put("payload", transfersJSON);

        new JSONResponseHttpHandler(response).handleRequest(exchange);
    }

    private TransferSearchArgument prepareTransferSearchArgument(final JSONObject payload) {
        final TransferSearchArgument searchArgument = new TransferSearchArgument();

        if (!payload.has("query")) {
            return searchArgument;
        }

        final JSONObject query = payload.getJSONObject("query");

        if (query.has("source")) {
            searchArgument.setSource(UUID.fromString(query.getString("source")));
        }

        if (query.has("destination")) {
            searchArgument.setDestination(UUID.fromString(query.getString("destination")));
        }

        if (query.has("dateFrom")) {
            searchArgument.setDateFrom(new Date(query.getLong("dateFrom")));
        }

        if (query.has("dateTo")) {
            searchArgument.setDateTo(new Date(query.getLong("dateTo")));
        }

        return searchArgument;
    }
}