package pl.maciejkrol.revolut.accountapi.web.handler;

import pl.maciejkrol.revolut.accountapi.dao.AccountDAO;
import pl.maciejkrol.revolut.accountapi.dao.fault.account.AccountNotFoundException;
import pl.maciejkrol.revolut.accountapi.entity.Account;
import pl.maciejkrol.revolut.accountapi.web.handler.general.JSONResponseHttpHandler;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import org.json.JSONObject;

import java.util.ArrayDeque;
import java.util.UUID;

public class AccountGetHttpHandler implements HttpHandler {

    private final AccountDAO accountDAO;

    public AccountGetHttpHandler(final AccountDAO accountDAO) {
        this.accountDAO = accountDAO;
    }

    @Override
    public void handleRequest(final HttpServerExchange exchange) throws AccountNotFoundException {
        final String uuidString = exchange
                .getQueryParameters()
                .getOrDefault("uuid", new ArrayDeque<>())
                .getFirst();

        final UUID uuid = UUID.fromString(uuidString);
        final Account account = this.accountDAO.get(uuid);

        final JSONObject json = new JSONObject()
                .put("success", true)
                .put("payload", new JSONObject()
                        .put("uuid", account.getUuid().toString())
                        .put("balance", account.getBalance())
                );

        new JSONResponseHttpHandler(json).handleRequest(exchange);
    }
}