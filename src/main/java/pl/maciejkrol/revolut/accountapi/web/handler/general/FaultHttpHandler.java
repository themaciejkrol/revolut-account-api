package pl.maciejkrol.revolut.accountapi.web.handler.general;

import io.undertow.server.HttpServerExchange;
import org.json.JSONObject;

public class FaultHttpHandler implements JSONPayloadHTTPHandler {

    private final String message;
    private final int code;

    public FaultHttpHandler(final String message, final int code) {
        this.message = message;
        this.code = code;
    }

    @Override
    public void handleRequest(final HttpServerExchange exchange) {
        final JSONObject response = new JSONObject()
                .put("success", false)
                .put("error", message);

        new JSONResponseHttpHandler(response, this.code).handleRequest(exchange);
    }
}