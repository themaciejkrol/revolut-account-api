package pl.maciejkrol.revolut.accountapi.web.handler.general;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;

public class NonBlockingHttpHandler implements HttpHandler {

    private final HttpHandler handler;

    public NonBlockingHttpHandler(final HttpHandler handler) {
        this.handler = handler;
    }

    @Override
    public void handleRequest(final HttpServerExchange exchange) throws Exception {
        if (exchange.isInIoThread()) {
            exchange.dispatch(this.handler);
        } else {
            this.handler.handleRequest(exchange);
        }
    }
}