package pl.maciejkrol.revolut.accountapi.web.handler;

import pl.maciejkrol.revolut.accountapi.dao.AccountDAO;
import pl.maciejkrol.revolut.accountapi.dao.fault.account.AccountCreationException;
import pl.maciejkrol.revolut.accountapi.entity.Account;
import pl.maciejkrol.revolut.accountapi.web.handler.general.JSONResponseHttpHandler;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import org.json.JSONObject;

public class AccountOpenHttpHandler implements HttpHandler {

    private final AccountDAO accountDAO;

    public AccountOpenHttpHandler(final AccountDAO accountDAO) {
        this.accountDAO = accountDAO;
    }

    @Override
    public void handleRequest(final HttpServerExchange exchange) throws AccountCreationException {
        final Account account = this.accountDAO.openAccount();

        final JSONObject json = new JSONObject()
                .put("success", true)
                .put("payload", new JSONObject()
                        .put("uuid", account.getUuid().toString())
                        .put("balance", account.getBalance())
                );

        new JSONResponseHttpHandler(json).handleRequest(exchange);
    }
}