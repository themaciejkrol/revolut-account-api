package pl.maciejkrol.revolut.accountapi.web.fault;

public class MalformedPayloadException extends Exception {

    public MalformedPayloadException() {
    }

    public MalformedPayloadException(final Throwable cause) {
        super(cause);
    }
}