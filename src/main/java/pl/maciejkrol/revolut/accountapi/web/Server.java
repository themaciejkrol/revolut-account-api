package pl.maciejkrol.revolut.accountapi.web;

import pl.maciejkrol.revolut.accountapi.dao.TransferDAO;
import pl.maciejkrol.revolut.accountapi.dao.fault.account.AccountCreationException;
import pl.maciejkrol.revolut.accountapi.dao.fault.account.AccountNotFoundException;
import pl.maciejkrol.revolut.accountapi.dao.fault.account.InsufficientFundsException;
import pl.maciejkrol.revolut.accountapi.dao.fault.account.InvalidAmountException;
import pl.maciejkrol.revolut.accountapi.service.TransferService;
import pl.maciejkrol.revolut.accountapi.web.fault.MalformedPayloadException;
import pl.maciejkrol.revolut.accountapi.web.fault.UnsupportedMethodException;
import pl.maciejkrol.revolut.accountapi.web.handler.*;
import pl.maciejkrol.revolut.accountapi.web.handler.general.FaultHttpHandler;
import pl.maciejkrol.revolut.accountapi.web.handler.general.NonBlockingHttpHandler;
import io.undertow.Handlers;
import io.undertow.Undertow;
import pl.maciejkrol.revolut.accountapi.dao.AccountDAO;

import java.io.IOException;

public class Server {

    public static Undertow.Builder prepare(
            final AccountDAO accountDAO,
            final TransferDAO transferDAO,
            final TransferService transferService
    ) {
        return Undertow.builder()
                .setHandler(new NonBlockingHttpHandler(
                        Handlers.exceptionHandler(Handlers.path()
                                .addPrefixPath("/account", Handlers.routing()
                                        .get("/open", new AccountOpenHttpHandler(accountDAO))
                                        .post("/list", new AccountListHttpHandler(accountDAO))
                                        .get("/get/{uuid}", new AccountGetHttpHandler(accountDAO))
                                        .post("/transfer", new TransferHttpHandler(transferService))
                                        .post("/transfer/list", new TransferListHttpHandler(transferDAO))
                                        .setFallbackHandler(new FaultHttpHandler("Invalid URL.", 404))
                                        .setInvalidMethodHandler(exchange -> {
                                            throw new UnsupportedMethodException();
                                        })
                                ))
                                .addExceptionHandler(AccountNotFoundException.class, new FaultHttpHandler("Account was not found.", 404))
                                .addExceptionHandler(IllegalArgumentException.class, new FaultHttpHandler("Invalid data.", 400))
                                .addExceptionHandler(UnsupportedMethodException.class, new FaultHttpHandler("Unsupported HTTP method.", 400))
                                .addExceptionHandler(MalformedPayloadException.class, new FaultHttpHandler("Invalid JSON or JSON format.", 400))
                                .addExceptionHandler(InvalidAmountException.class, new FaultHttpHandler("Invalid amount provided.", 400))
                                .addExceptionHandler(InsufficientFundsException.class, new FaultHttpHandler("Insufficient funds on the source account.", 403))
                                .addExceptionHandler(AccountCreationException.class, new FaultHttpHandler("Creating account failed.", 500))
                                .addExceptionHandler(IOException.class, new FaultHttpHandler("Retrieving payload failed..", 500))
                ));
    }
}