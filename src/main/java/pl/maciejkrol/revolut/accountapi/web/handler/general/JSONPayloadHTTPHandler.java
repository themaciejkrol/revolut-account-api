package pl.maciejkrol.revolut.accountapi.web.handler.general;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import org.json.JSONException;
import org.json.JSONObject;
import pl.maciejkrol.revolut.accountapi.web.fault.MalformedPayloadException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public interface JSONPayloadHTTPHandler extends HttpHandler {

    default JSONObject getPayload(final HttpServerExchange exchange) throws IOException, MalformedPayloadException {
        try {
            exchange.startBlocking();

            try (
                    final InputStream inputStream = exchange.getInputStream();
                    final InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                    final BufferedReader bufferedReader = new BufferedReader(inputStreamReader)
            ) {
                final StringBuilder stringBuilder = new StringBuilder();
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line);
                }
                return new JSONObject(stringBuilder.toString());
            }
        } catch (JSONException e) {
            throw new MalformedPayloadException(e);
        }
    }
}