package pl.maciejkrol.revolut.accountapi.web.handler.general;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;
import org.json.JSONObject;

public class JSONResponseHttpHandler implements HttpHandler {

    private final int code;
    private final JSONObject json;

    public JSONResponseHttpHandler(final JSONObject json, final int code) {
        this.json = json;
        this.code = code;
    }

    public JSONResponseHttpHandler(final JSONObject json) {
        this.json = json;
        this.code = 200;
    }

    @Override
    public void handleRequest(final HttpServerExchange exchange) {

        exchange
                .getResponseHeaders()
                .add(Headers.CONTENT_TYPE, "application/json");

        exchange
                .setStatusCode(this.code)
                .getResponseSender()
                .send(this.json.toString());

        exchange.endExchange();
    }
}