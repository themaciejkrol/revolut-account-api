package pl.maciejkrol.revolut.accountapi.service;

import pl.maciejkrol.revolut.accountapi.dao.fault.AccountException;
import pl.maciejkrol.revolut.accountapi.dao.fault.TransferException;
import pl.maciejkrol.revolut.accountapi.entity.Transfer;

import java.util.UUID;

public interface TransferService {
    Transfer transfer(UUID source, UUID destination, long amount) throws TransferException, AccountException;
}