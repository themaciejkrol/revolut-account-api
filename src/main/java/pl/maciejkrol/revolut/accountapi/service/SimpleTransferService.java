package pl.maciejkrol.revolut.accountapi.service;

import pl.maciejkrol.revolut.accountapi.dao.AccountDAO;
import pl.maciejkrol.revolut.accountapi.dao.TransferDAO;
import pl.maciejkrol.revolut.accountapi.dao.fault.AccountException;
import pl.maciejkrol.revolut.accountapi.dao.fault.TransferException;
import pl.maciejkrol.revolut.accountapi.entity.Transfer;

import java.util.UUID;

public class SimpleTransferService implements TransferService {

    private final AccountDAO accountDAO;
    private final TransferDAO transferDAO;

    public SimpleTransferService(final AccountDAO accountDAO, final TransferDAO transferDAO) {
        this.accountDAO = accountDAO;
        this.transferDAO = transferDAO;
    }

    @Override
    public Transfer transfer(final UUID source, final UUID destination, final long amount) throws TransferException, AccountException {
        this.accountDAO.transfer(source, destination, amount);

        try {
            return this.transferDAO.register(source, destination, amount);
        } catch (final TransferException e) {
            this.accountDAO.transfer(destination, source, amount, true);
            throw new TransferException(e);
        }
    }
}