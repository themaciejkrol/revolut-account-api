package pl.maciejkrol.revolut.accountapi.common;

import pl.maciejkrol.revolut.accountapi.Main;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;

public class ResourceUtil {

    private static final int DEFAULT_BUFFER_SIZE = 1024 * 4;

    public static String toString(final String name) throws IOException {
        try (
                final StringWriter stringWriter = new StringWriter();
                final InputStreamReader inputStreamReader = new InputStreamReader(Main.class.getResourceAsStream(name))
        ) {
            final char[] buffer = new char[DEFAULT_BUFFER_SIZE];
            int n;
            while (-1 != (n = inputStreamReader.read(buffer))) {
                stringWriter.write(buffer, 0, n);
            }

            return stringWriter.toString();
        }
    }
}