package pl.maciejkrol.revolut.accountapi.entity;

import java.util.UUID;

public final class Account {

    private final UUID uuid;
    private final long balance;

    public Account(final UUID uuid, final long balance) {
        if (uuid == null) {
            throw new NullPointerException();
        }

        this.uuid = new UUID(uuid.getMostSignificantBits(), uuid.getLeastSignificantBits());
        this.balance = balance;
    }

    public UUID getUuid() {
        return new UUID(
                this.uuid.getMostSignificantBits(),
                this.uuid.getLeastSignificantBits()
        );
    }

    public long getBalance() {
        return this.balance;
    }

    public static Account clone(final Account other) {
        return new Account(
                other.getUuid(),
                other.getBalance()
        );
    }
}