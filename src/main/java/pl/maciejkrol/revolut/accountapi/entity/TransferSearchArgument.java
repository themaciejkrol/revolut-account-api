package pl.maciejkrol.revolut.accountapi.entity;

import java.util.Date;
import java.util.UUID;

public class TransferSearchArgument {

    private UUID source;
    private UUID destination;
    private Date dateFrom;
    private Date dateTo;

    public UUID getSource() {
        if (this.source == null) {
            return null;
        }

        return new UUID(this.source.getMostSignificantBits(), this.source.getLeastSignificantBits());
    }

    public void setSource(final UUID source) {
        this.source = source == null
                ? null
                : new UUID(source.getMostSignificantBits(), source.getLeastSignificantBits());
    }

    public UUID getDestination() {
        if (this.destination == null) {
            return null;
        }

        return new UUID(this.destination.getMostSignificantBits(), this.destination.getLeastSignificantBits());
    }

    public void setDestination(final UUID destination) {
        this.destination = destination == null
                ? null
                : new UUID(destination.getMostSignificantBits(), destination.getLeastSignificantBits());
    }

    public Date getDateFrom() {
        if (this.dateFrom == null) {
            return null;
        }

        return new Date(this.dateFrom.getTime());
    }

    public void setDateFrom(final Date dateFrom) {
        this.dateFrom = dateFrom == null
                ? null
                : new Date(dateFrom.getTime());
    }

    public Date getDateTo() {
        if (this.dateTo == null) {
            return null;
        }

        return new Date(this.dateTo.getTime());
    }

    public void setDateTo(final Date dateTo) {
        this.dateTo = dateTo == null
                ? null
                : new Date(dateTo.getTime());
    }
}