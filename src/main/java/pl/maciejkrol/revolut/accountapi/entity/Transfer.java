package pl.maciejkrol.revolut.accountapi.entity;

import java.util.Date;
import java.util.UUID;

public final class Transfer {

    private final UUID uuid;
    private final UUID source;
    private final UUID destination;
    private final Date date;
    private final long amount;

    public Transfer(
            final UUID uuid,
            final UUID source,
            final UUID destination,
            final Date date,
            final long amount
    ) {
        if (uuid == null || source == null || destination == null || date == null) {
            throw new NullPointerException();
        }

        this.uuid = new UUID(uuid.getMostSignificantBits(), uuid.getLeastSignificantBits());
        this.source = new UUID(source.getMostSignificantBits(), source.getLeastSignificantBits());
        this.destination = new UUID(destination.getMostSignificantBits(), destination.getLeastSignificantBits());
        this.date = new Date(date.getTime());
        this.amount = amount;
    }

    public UUID getUuid() {
        return new UUID(
                this.uuid.getMostSignificantBits(),
                this.uuid.getLeastSignificantBits()
        );
    }

    public UUID getSource() {
        return new UUID(
                this.source.getMostSignificantBits(),
                this.source.getLeastSignificantBits()
        );
    }

    public UUID getDestination() {
        return new UUID(
                this.destination.getMostSignificantBits(),
                this.destination.getLeastSignificantBits()
        );
    }

    public Date getDate() {
        return new Date(this.date.getTime());
    }

    public long getAmount() {
        return this.amount;
    }

    public static Transfer clone(final Transfer transfer) {
        return new Transfer(
                transfer.getUuid(),
                transfer.getSource(),
                transfer.getDestination(),
                transfer.getDate(),
                transfer.getAmount()
        );
    }
}