package pl.maciejkrol.revolut.accountapi.server;

import io.undertow.Undertow;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import pl.maciejkrol.revolut.accountapi.dao.AccountDAO;
import pl.maciejkrol.revolut.accountapi.dao.TransferDAO;
import pl.maciejkrol.revolut.accountapi.dao.inmemory.InMemoryAccountDAO;
import pl.maciejkrol.revolut.accountapi.dao.inmemory.InMemoryTransferDAO;
import pl.maciejkrol.revolut.accountapi.entity.Account;
import pl.maciejkrol.revolut.accountapi.service.SimpleTransferService;
import pl.maciejkrol.revolut.accountapi.service.TransferService;
import pl.maciejkrol.revolut.accountapi.web.Server;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SuppressWarnings({"WeakerAccess", "FieldCanBeLocal"})
public class ServerTransferTest extends ServerTest {

    private static int PORT = 8083;

    @Test
    public void testServer() throws Exception {
        final UUID sourceUuid = UUID.fromString("957f2f62-2f0d-474c-8a14-f35909f0fd84");
        final UUID destinationUuid = UUID.fromString("5abdcaaf-1cde-4c24-97c5-5b6945c0dce2");

        final long sourceTotal = 200;
        final long destinationTotal = 10;
        final long amount = 58;

        final Set<Account> accounts = new HashSet<>();
        accounts.add(new Account(sourceUuid, sourceTotal));
        accounts.add(new Account(destinationUuid, destinationTotal));

        final AccountDAO accountDAO = new InMemoryAccountDAO(accounts);
        final TransferDAO transferDAO = new InMemoryTransferDAO();
        final TransferService transferService = new SimpleTransferService(accountDAO, transferDAO);

        final Undertow undertow = Server.prepare(accountDAO, transferDAO, transferService)
                .addHttpListener(PORT, HOST)
                .setIoThreads(1)
                .setWorkerThreads(1)
                .build();

        undertow.start();

        final JSONObject transferRequestJSON = new JSONObject()
                .put("source", sourceUuid.toString())
                .put("destination", destinationUuid.toString())
                .put("amount", amount);

        final JSONObject transferResponseJSON = testPOST_HTTPResponse(
                prepareUrl(PORT, URL_ACCOUNT_TRANSFER),
                transferRequestJSON.toString(),
                200,
                false
        );

        assertTrue(transferResponseJSON.getBoolean("success"));

        final Account sourceAccount = accountDAO.get(sourceUuid);
        final Account destinationAccount = accountDAO.get(destinationUuid);

        assertEquals(sourceTotal - amount, sourceAccount.getBalance());
        assertEquals(destinationTotal + amount, destinationAccount.getBalance());

        final JSONObject transferListRequestJSON = new JSONObject()
                .put("limit", 100);

        final JSONObject transferListResponseJSON = testPOST_HTTPResponse(
                prepareUrl(PORT, URL_ACCOUNT_TRANSFER_LIST),
                transferListRequestJSON.toString(),
                200,
                false
        );

        assertTrue(transferListResponseJSON.getBoolean("success"));

        final JSONArray transferListArray = transferListResponseJSON.getJSONArray("payload");

        assertEquals(1, transferListArray.length());

        final JSONObject transferJSON = transferListArray.getJSONObject(0);

        assertEquals(sourceUuid, UUID.fromString(transferJSON.getString("source")));
        assertEquals(destinationUuid, UUID.fromString(transferJSON.getString("destination")));
        assertEquals(amount, transferJSON.getLong("amount"));

        undertow.stop();
    }
}