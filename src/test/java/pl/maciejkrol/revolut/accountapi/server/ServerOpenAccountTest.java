package pl.maciejkrol.revolut.accountapi.server;

import pl.maciejkrol.revolut.accountapi.dao.AccountDAO;
import pl.maciejkrol.revolut.accountapi.dao.TransferDAO;
import pl.maciejkrol.revolut.accountapi.dao.inmemory.InMemoryAccountDAO;
import pl.maciejkrol.revolut.accountapi.dao.inmemory.InMemoryTransferDAO;
import pl.maciejkrol.revolut.accountapi.service.SimpleTransferService;
import pl.maciejkrol.revolut.accountapi.service.TransferService;
import pl.maciejkrol.revolut.accountapi.web.Server;
import io.undertow.Undertow;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SuppressWarnings({"WeakerAccess", "FieldCanBeLocal"})
public class ServerOpenAccountTest extends ServerTest {

    private static int PORT = 8082;

    @Test
    public void testServer() throws Exception {
        final AccountDAO accountDAO = new InMemoryAccountDAO();
        final TransferDAO transferDAO = new InMemoryTransferDAO();
        final TransferService transferService = new SimpleTransferService(accountDAO, transferDAO);

        final Undertow undertow = Server.prepare(accountDAO, transferDAO, transferService)
                .addHttpListener(PORT, HOST)
                .setIoThreads(1)
                .setWorkerThreads(1)
                .build();

        undertow.start();

        final JSONObject responseOpen = testGET_HTTPResponse(prepareUrl(PORT, URL_ACCOUNT_OPEN), 200, false);
        assertTrue(responseOpen.getBoolean("success"));

        final JSONObject payloadOpen = responseOpen.getJSONObject("payload");

        assertEquals(0, payloadOpen.getInt("balance"));
        final UUID uuid = UUID.fromString(payloadOpen.getString("uuid"));

        final JSONObject responseGet = testGET_HTTPResponse(prepareUrl(PORT, URL_ACCOUNT_GET + uuid.toString()), 200, false);

        assertTrue(responseGet.getBoolean("success"));

        final JSONObject payloadGet = responseGet.getJSONObject("payload");

        assertEquals(0, payloadGet.getInt("balance"));
        assertEquals(uuid, UUID.fromString(payloadGet.getString("uuid")));

        undertow.stop();
    }
}