package pl.maciejkrol.revolut.accountapi.server;

import io.undertow.Undertow;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import pl.maciejkrol.revolut.accountapi.dao.AccountDAO;
import pl.maciejkrol.revolut.accountapi.dao.TransferDAO;
import pl.maciejkrol.revolut.accountapi.dao.inmemory.InMemoryAccountDAO;
import pl.maciejkrol.revolut.accountapi.dao.inmemory.InMemoryTransferDAO;
import pl.maciejkrol.revolut.accountapi.entity.Account;
import pl.maciejkrol.revolut.accountapi.service.SimpleTransferService;
import pl.maciejkrol.revolut.accountapi.service.TransferService;
import pl.maciejkrol.revolut.accountapi.web.Server;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SuppressWarnings({"WeakerAccess", "FieldCanBeLocal"})
public class ServerGetAccountTest extends ServerTest {

    private static int PORT = 8080;

    @Test
    public void testServer() throws Exception {
        final UUID uuid = UUID.fromString("957f2f62-2f0d-474c-8a14-f35909f0fd84");
        final int balance = 20;

        final Set<Account> accounts = new HashSet<>();
        accounts.add(new Account(uuid, balance));

        final AccountDAO accountDAO = new InMemoryAccountDAO(accounts);
        final TransferDAO transferDAO = new InMemoryTransferDAO();
        final TransferService transferService = new SimpleTransferService(accountDAO, transferDAO);

        final Undertow undertow = Server.prepare(accountDAO, transferDAO, transferService)
                .addHttpListener(PORT, HOST)
                .setIoThreads(1)
                .setWorkerThreads(1)
                .build();

        undertow.start();

        final JSONObject response = testGET_HTTPResponse(prepareUrl(PORT, URL_ACCOUNT_GET + uuid.toString()), 200, false);
        assertTrue(response.getBoolean("success"));

        final JSONObject payload = response.getJSONObject("payload");

        assertEquals(balance, payload.getInt("balance"));
        assertEquals(uuid, UUID.fromString(payload.getString("uuid")));

        undertow.stop();
    }
}