package pl.maciejkrol.revolut.accountapi.server;

import pl.maciejkrol.revolut.accountapi.dao.AccountDAO;
import pl.maciejkrol.revolut.accountapi.dao.TransferDAO;
import pl.maciejkrol.revolut.accountapi.dao.inmemory.InMemoryAccountDAO;
import pl.maciejkrol.revolut.accountapi.dao.inmemory.InMemoryTransferDAO;
import pl.maciejkrol.revolut.accountapi.service.SimpleTransferService;
import pl.maciejkrol.revolut.accountapi.service.TransferService;
import pl.maciejkrol.revolut.accountapi.web.Server;
import io.undertow.Undertow;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertFalse;

@SuppressWarnings({"WeakerAccess", "FieldCanBeLocal"})
public class ServerGetNonExistentAccountTest extends ServerTest {

    private static int PORT = 8081;

    @Test
    public void testServer() throws Exception {
        final AccountDAO accountDAO = new InMemoryAccountDAO();
        final TransferDAO transferDAO = new InMemoryTransferDAO();
        final TransferService transferService = new SimpleTransferService(accountDAO, transferDAO);

        final UUID uuid = new UUID(0, 0);

        final Undertow undertow = Server.prepare(accountDAO, transferDAO, transferService)
                .addHttpListener(PORT, HOST)
                .setIoThreads(1)
                .setWorkerThreads(1)
                .build();

        undertow.start();

        final JSONObject response = testGET_HTTPResponse(prepareUrl(PORT, URL_ACCOUNT_GET + uuid.toString()), 404, true);

        assertFalse(response.getBoolean("success"));

        undertow.stop();
    }
}