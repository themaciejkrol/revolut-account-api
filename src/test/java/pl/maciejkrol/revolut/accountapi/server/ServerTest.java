package pl.maciejkrol.revolut.accountapi.server;

import org.json.JSONObject;
import org.junit.jupiter.api.TestInstance;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SuppressWarnings("WeakerAccess")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public abstract class ServerTest {

    static String HOST = "127.0.0.1";

    static String URL_ACCOUNT_GET = "account/get/";
    static String URL_ACCOUNT_OPEN = "account/open";
    static String URL_ACCOUNT_TRANSFER = "account/transfer";
    static String URL_ACCOUNT_TRANSFER_LIST = "account/transfer/list";

    static String prepareUrl(final int port, final String path) {
        return "http://" + HOST + ":" + port + "/" + path;
    }

    static JSONObject testPOST_HTTPResponse(final String path, final String payload, final int assertCode, final boolean errorStream) throws IOException {
        final URL url = new URL(path);
        final HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setDoOutput(true);
        connection.setRequestMethod("POST");

        try (
                final OutputStream outputStream = connection.getOutputStream();
                final OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, "UTF-8");
        ) {
            outputStreamWriter.write(payload);
            outputStreamWriter.flush();
        }

        assertEquals(assertCode, connection.getResponseCode());
        final JSONObject response = new JSONObject(readResponse(connection, errorStream));

        connection.disconnect();
        return response;
    }


    static JSONObject testGET_HTTPResponse(final String path, final int assertCode, final boolean errorStream) throws IOException {
        final URL url = new URL(path);
        final HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");

        assertEquals(assertCode, connection.getResponseCode());
        final JSONObject response = new JSONObject(readResponse(connection, errorStream));

        connection.disconnect();
        return response;
    }

    static String readResponse(final HttpURLConnection connection, final boolean errorStream) throws IOException {
        try (
                final InputStream inputStream = errorStream
                        ? connection.getErrorStream()
                        : connection.getInputStream();
                final Reader streamReader = new InputStreamReader(inputStream);
                final BufferedReader bufferedReader = new BufferedReader(streamReader);
        ) {
            String inputLine;
            final StringBuilder content = new StringBuilder();
            while ((inputLine = bufferedReader.readLine()) != null) {
                content.append(inputLine);
            }
            return content.toString();
        }
    }
}