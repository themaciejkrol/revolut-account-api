package pl.maciejkrol.revolut.accountapi.dao.transfer;

import pl.maciejkrol.revolut.accountapi.dao.TransferDAO;
import pl.maciejkrol.revolut.accountapi.dao.fault.transfer.TransferCreationException;
import pl.maciejkrol.revolut.accountapi.dao.fault.transfer.TransferNotFoundException;
import pl.maciejkrol.revolut.accountapi.dao.inmemory.InMemoryTransferDAO;
import pl.maciejkrol.revolut.accountapi.entity.Transfer;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings("WeakerAccess")
public class InMemoryTransferDAOTest {

    @Test
    public void testRegisterTransfer() throws TransferCreationException {
        final UUID sourceUuid = UUID.fromString("957f2f62-2f0d-474c-8a14-f35909f0fd84");
        final UUID destinationUuid = UUID.fromString("5abdcaaf-1cde-4c24-97c5-5b6945c0dce2");
        final long amount = 500;

        final TransferDAO transferDAO = new InMemoryTransferDAO();
        final Transfer transfer = transferDAO.register(sourceUuid, destinationUuid, amount);

        assertNotNull(transfer);
        assertEquals(sourceUuid, transfer.getSource());
        assertEquals(destinationUuid, transfer.getDestination());
        assertEquals(amount, transfer.getAmount());

        assertTrue(transferDAO.exists(transfer.getUuid()));
    }

    @Test
    public void testGetNonExistentTransfer() {
        final TransferDAO transferDAO = new InMemoryTransferDAO();
        final UUID uuid = new UUID(0L, 0L);

        assertFalse(transferDAO.exists(uuid));
        assertThrows(TransferNotFoundException.class, () -> transferDAO.get(uuid));
    }
}