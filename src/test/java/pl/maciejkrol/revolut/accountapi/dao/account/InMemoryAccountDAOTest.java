package pl.maciejkrol.revolut.accountapi.dao.account;

import org.junit.jupiter.api.Test;
import pl.maciejkrol.revolut.accountapi.dao.AccountDAO;
import pl.maciejkrol.revolut.accountapi.dao.fault.TransferException;
import pl.maciejkrol.revolut.accountapi.dao.fault.account.AccountCreationException;
import pl.maciejkrol.revolut.accountapi.dao.fault.account.AccountNotFoundException;
import pl.maciejkrol.revolut.accountapi.dao.fault.account.InsufficientFundsException;
import pl.maciejkrol.revolut.accountapi.dao.inmemory.InMemoryAccountDAO;
import pl.maciejkrol.revolut.accountapi.entity.Account;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings("WeakerAccess")
public class InMemoryAccountDAOTest {

    @Test
    public void testCreateAccount() throws AccountCreationException, AccountNotFoundException {
        final AccountDAO accountDAO = new InMemoryAccountDAO();
        final Account account = accountDAO.openAccount();

        assertNotNull(account);
        assertNotNull(account.getUuid());
        assertEquals(0, account.getBalance());

        assertTrue(accountDAO.exists(account.getUuid()));

        final Account account2 = accountDAO.get(account.getUuid());
        assertEquals(account.getUuid(), account2.getUuid());
        assertEquals(account.getBalance(), account2.getBalance());
    }

    @Test
    public void testGetNonExistentAccount() {
        final Set<Account> accounts = new HashSet<Account>();
        accounts.add(new Account(UUID.fromString("957f2f62-2f0d-474c-8a14-f35909f0fd84"), 0));

        final AccountDAO accountDAO = new InMemoryAccountDAO(accounts);
        final UUID uuid = new UUID(0L, 0L);

        assertFalse(accountDAO.exists(uuid));
        assertThrows(AccountNotFoundException.class, () -> accountDAO.get(uuid));
    }

    @Test
    public void testTransfer() throws AccountNotFoundException, TransferException {
        final UUID uuid1 = UUID.fromString("957f2f62-2f0d-474c-8a14-f35909f0fd84");
        final UUID uuid2 = UUID.fromString("5abdcaaf-1cde-4c24-97c5-5b6945c0dce2");

        final long total1 = 0;
        final long total2 = 200;
        final long transfer = 58;

        final Set<Account> accounts = new HashSet<>();
        accounts.add(new Account(uuid1, total1));
        accounts.add(new Account(uuid2, total2));

        final AccountDAO accountDAO = new InMemoryAccountDAO(accounts);

        assertEquals(total1, accountDAO.get(uuid1).getBalance());
        assertEquals(total2, accountDAO.get(uuid2).getBalance());

        accountDAO.transfer(uuid2, uuid1, transfer);

        assertEquals(total1 + transfer, accountDAO.get(uuid1).getBalance());
        assertEquals(total2 - transfer, accountDAO.get(uuid2).getBalance());
    }

    @Test
    public void testTransferInsufficientFunds() throws AccountNotFoundException {
        final UUID uuid1 = UUID.fromString("957f2f62-2f0d-474c-8a14-f35909f0fd84");
        final UUID uuid2 = UUID.fromString("5abdcaaf-1cde-4c24-97c5-5b6945c0dce2");

        final long total1 = 5;
        final long total2 = 200;
        final long transfer = total2 + 10;

        final Set<Account> accounts = new HashSet<Account>() {{
            this.add(new Account(uuid1, total1));
            this.add(new Account(uuid2, total2));
        }};

        final AccountDAO accountDAO = new InMemoryAccountDAO(accounts);

        assertEquals(total1, accountDAO.get(uuid1).getBalance());
        assertEquals(total2, accountDAO.get(uuid2).getBalance());

        assertThrows(InsufficientFundsException.class, () -> accountDAO.transfer(uuid2, uuid1, transfer));

        assertEquals(total1, accountDAO.get(uuid1).getBalance());
        assertEquals(total2, accountDAO.get(uuid2).getBalance());

        assertThrows(InsufficientFundsException.class, () -> accountDAO.transfer(uuid1, uuid2, transfer));

        assertEquals(total1, accountDAO.get(uuid1).getBalance());
        assertEquals(total2, accountDAO.get(uuid2).getBalance());
    }
}