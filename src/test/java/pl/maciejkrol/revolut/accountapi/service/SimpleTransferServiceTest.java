package pl.maciejkrol.revolut.accountapi.service;

import pl.maciejkrol.revolut.accountapi.dao.AccountDAO;
import pl.maciejkrol.revolut.accountapi.dao.TransferDAO;
import pl.maciejkrol.revolut.accountapi.dao.fault.AccountException;
import pl.maciejkrol.revolut.accountapi.dao.fault.TransferException;
import pl.maciejkrol.revolut.accountapi.dao.fault.account.AccountNotFoundException;
import pl.maciejkrol.revolut.accountapi.dao.fault.account.InsufficientFundsException;
import pl.maciejkrol.revolut.accountapi.dao.inmemory.InMemoryAccountDAO;
import pl.maciejkrol.revolut.accountapi.dao.inmemory.InMemoryTransferDAO;
import pl.maciejkrol.revolut.accountapi.entity.Account;
import pl.maciejkrol.revolut.accountapi.entity.Transfer;
import pl.maciejkrol.revolut.accountapi.entity.TransferSearchArgument;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SuppressWarnings("WeakerAccess")
public class SimpleTransferServiceTest {

    @Test
    public void testTransfer() throws AccountException, TransferException {
        final UUID sourceUuid = UUID.fromString("957f2f62-2f0d-474c-8a14-f35909f0fd84");
        final UUID destinationUuid = UUID.fromString("5abdcaaf-1cde-4c24-97c5-5b6945c0dce2");

        final long sourceTotal = 200;
        final long destinationTotal = 0;
        final long amount = 58;

        final Set<Account> accounts = new HashSet<>();
        accounts.add(new Account(sourceUuid, sourceTotal));
        accounts.add(new Account(destinationUuid, destinationTotal));

        final AccountDAO accountDAO = new InMemoryAccountDAO(accounts);
        final TransferDAO transferDAO = new InMemoryTransferDAO();
        final TransferService transferService = new SimpleTransferService(accountDAO, transferDAO);

        assertEquals(0, transferDAO.list(new TransferSearchArgument(), 0, 10).size());
        final Transfer transfer = transferService.transfer(sourceUuid, destinationUuid, amount);
        assertEquals(1, transferDAO.list(new TransferSearchArgument(), 0, 10).size());

        assertEquals(sourceUuid, transfer.getSource());
        assertEquals(destinationUuid, transfer.getDestination());
        assertEquals(amount, transfer.getAmount());

        final Account sourceAccount = accountDAO.get(sourceUuid);
        final Account destinationAccount = accountDAO.get(destinationUuid);

        assertEquals(sourceUuid, sourceAccount.getUuid());
        assertEquals(destinationUuid, destinationAccount.getUuid());
        assertEquals(sourceTotal - amount, sourceAccount.getBalance());
        assertEquals(destinationTotal + amount, destinationAccount.getBalance());
    }

    @Test
    public void testTransferNonExistentAccount() throws AccountException {
        final UUID sourceUuid = new UUID(0, 0);
        final UUID destinationUuid = UUID.fromString("5abdcaaf-1cde-4c24-97c5-5b6945c0dce2");

        final long destinationTotal = 0;
        final long amount = 58;

        final Set<Account> accounts = new HashSet<>();
        accounts.add(new Account(destinationUuid, destinationTotal));

        final AccountDAO accountDAO = new InMemoryAccountDAO(accounts);
        final TransferDAO transferDAO = new InMemoryTransferDAO();
        final TransferService transferService = new SimpleTransferService(accountDAO, transferDAO);

        assertEquals(0, transferDAO.list(new TransferSearchArgument(), 0, 10).size());
        assertThrows(AccountNotFoundException.class, () -> transferService.transfer(sourceUuid, destinationUuid, amount));
        assertThrows(AccountNotFoundException.class, () -> accountDAO.get(sourceUuid));

        final Account destinationAccount = accountDAO.get(destinationUuid);

        assertEquals(0, transferDAO.list(new TransferSearchArgument(), 0, 10).size());
        assertEquals(destinationUuid, destinationAccount.getUuid());
        assertEquals(destinationTotal, destinationAccount.getBalance());
    }

    @Test
    public void testTransferInsufficientFundsException() throws AccountException {
        final UUID sourceUuid = UUID.fromString("957f2f62-2f0d-474c-8a14-f35909f0fd84");
        final UUID destinationUuid = UUID.fromString("5abdcaaf-1cde-4c24-97c5-5b6945c0dce2");

        final long sourceTotal = 0;
        final long destinationTotal = 200;
        final long amount = 58;

        final Set<Account> accounts = new HashSet<>();
        accounts.add(new Account(sourceUuid, sourceTotal));
        accounts.add(new Account(destinationUuid, destinationTotal));

        final AccountDAO accountDAO = new InMemoryAccountDAO(accounts);
        final TransferDAO transferDAO = new InMemoryTransferDAO();
        final TransferService transferService = new SimpleTransferService(accountDAO, transferDAO);

        assertEquals(0, transferDAO.list(new TransferSearchArgument(), 0, 10).size());
        assertThrows(InsufficientFundsException.class, () -> transferService.transfer(sourceUuid, destinationUuid, amount));
        assertEquals(0, transferDAO.list(new TransferSearchArgument(), 0, 10).size());

        final Account sourceAccount = accountDAO.get(sourceUuid);
        final Account destinationAccount = accountDAO.get(destinationUuid);

        assertEquals(sourceUuid, sourceAccount.getUuid());
        assertEquals(destinationUuid, destinationAccount.getUuid());
        assertEquals(sourceTotal, sourceAccount.getBalance());
        assertEquals(destinationTotal, destinationAccount.getBalance());
    }
}